require('dotenv').config();

import veglia from './veglia';
import startHandler from './handlers/startHandler';
import { TimeoutScheduler } from './infra/messageScheduler/timeout';
import { commitListner, tushiListner } from './handlers/messageListners';

// Timeout message schedueler
const timeoutScheduler = new TimeoutScheduler(console);

// Declare handlers
const startCommand = startHandler(timeoutScheduler);

// Setup bot instance
new veglia({
  polling: {
    autoStart: true,
  },
  telegramToken: String(process.env.TELEGRAM_TOKEN),
  logger: console,
})
.addHandler(startCommand)
.addHandler(commitListner)
.addHandler(tushiListner);
