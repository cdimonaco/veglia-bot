import nodeTelegramBotApi from 'node-telegram-bot-api';
import startHandler from './handlers/startHandler';
import { Handler, HandlerFunc } from './handlers';

export type VegliaConfig = {
  logger: any,
  telegramToken: string,
  handlers?: any[],
} & nodeTelegramBotApi.ConstructorOptions;

export default class Veglia {
  private apiInstance: nodeTelegramBotApi;
  private appLogger: any; // TODO: Make a logging interface
  private availableHandlers: Handler[] = [];

  constructor(options: VegliaConfig) {
    const { logger, ...apiOptions } = options;
    this.appLogger = logger;
    this.apiInstance = new nodeTelegramBotApi(options.telegramToken, apiOptions);
    this.appLogger.info(`Veglia initialized with token ${options.telegramToken}`);
    this.apiInstance.on('message', msg => this.appLogger.info(msg));
    return this;
  }

  public addHandler(handler: HandlerFunc) {
    const { commandMatcher, task } = handler(this.apiInstance, this.appLogger);
    this.apiInstance.onText(
      commandMatcher,
      task,
    );
    this.availableHandlers = [...this.availableHandlers, { commandMatcher, task }];
    this.appLogger.info(`Handler with regexp ${commandMatcher}, added`);
    return this;
  }
}
