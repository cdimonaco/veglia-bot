import nodeTelegramBotApi from 'node-telegram-bot-api';
import { Handler, HandlerFunc } from '..';

const messageHandler = (
  logger: any,
  bot: nodeTelegramBotApi,
  messageToSend: string,
  ) => (
    msg: nodeTelegramBotApi.Message,
) => {
    logger.info(`Sending ${messageToSend} to chat ${msg.chat.id}`);
    // Check if the the chat is group chat
    if (!(msg.chat.type === 'group')) return;
    bot.sendMessage(msg.chat.id, messageToSend);
  };

export const configureTextListner = (textMatcher: RegExp, messageToSend: string) :HandlerFunc => (
  api: nodeTelegramBotApi, logger: any,
): Handler  => ({
  commandMatcher: textMatcher,
  task: messageHandler(logger, api, messageToSend),
});
