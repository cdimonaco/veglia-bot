import { configureTextListner } from './generator';

const ON_TUSHI = 'TUSHI TUSHI, TE LO BATTO SUL GROPPONE EHEHEHEH';
const TUSHI_TEXT = /tushi/g;

const ON_COMMIT = 'git commit -m "update readme, solar config, my tfr destination, my gender"';
const COMMIT_TEXT = /commit/g;

export const commitListner = configureTextListner(COMMIT_TEXT, ON_COMMIT);
export const tushiListner = configureTextListner(TUSHI_TEXT, ON_TUSHI);
