export * from './startHandler';
import nodeTelegramBotApi from 'node-telegram-bot-api';

export type HandlerFunc = (api: nodeTelegramBotApi, logger: any) => Handler;
export type Handler = {
  commandMatcher: RegExp,
  task: (msg: nodeTelegramBotApi.Message, match: RegExpExecArray | null) => void,
};
