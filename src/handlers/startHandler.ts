import nodeTelegramBotApi from 'node-telegram-bot-api';
import { Handler, HandlerFunc } from '.';
import { MessageScheduler } from '../infra/messageScheduler';

const commandMatcher = /^\/vegliami$/g;
const TWO_MINUTES = 120000;
const VEGLIA_WELCOME_MESSAGE = 'Oh veglia!, tra due minuti ti veglio, diocane!';
const VEGLIA_BAD_WELCOME_MESSAGE = 'Diocane, che mi chiami a fare, che ti veglio da solo?';
const VEGLIA_USER_NOT_ACCESSIBLE = 'Cazzo me lo dici a fare? Non posso vegliarti imbecille!';

const messageHandler = (
  logger: any,
  bot: nodeTelegramBotApi,
  messageScheduler: MessageScheduler,
  ) => (
    msg: nodeTelegramBotApi.Message,
) => {
    logger.info(`Start command received from ${msg.chat.id}`);
    // Check if the the chat is group chat
    if (!(msg.chat.type === 'group')) {
      bot.sendMessage(msg.chat.id, VEGLIA_BAD_WELCOME_MESSAGE);
      return;
    }
    // Check if we have the from payload
    if (!msg.from) {
      bot.sendMessage(msg.chat.id, VEGLIA_USER_NOT_ACCESSIBLE);
      return;
    }
    bot.sendMessage(msg.chat.id, VEGLIA_WELCOME_MESSAGE);
    messageScheduler.schedule({
      message: `${msg.from.first_name} VEGLIA!`,
      when: new Date(new Date().getTime() + TWO_MINUTES),
      chatId: String(msg.chat.id),
      messageSender: bot,
    }).then(logger.info);
  };

const startHandler = (messageScheduler: MessageScheduler) :HandlerFunc => (
  api: nodeTelegramBotApi, logger: any,
): Handler  => ({
  commandMatcher,
  task: messageHandler(logger, api, messageScheduler),
});

export default startHandler;
