import { MessageScheduler, ScheduleMessagePayload, ScheduledJob } from './';
import nodeTelegramBotApi from 'node-telegram-bot-api';

type StoredJob = {
  timeout: NodeJS.Timeout,
} & ScheduledJob;

export class TimeoutScheduler implements MessageScheduler {
  private scheduledJobs: Map<String, StoredJob>;
  constructor(private logger: any) {
    this.scheduledJobs = new Map();
  }

  schedule({ message, when, chatId, messageSender }: ScheduleMessagePayload) {
    // Calculate milliseconds between now and when
    // than use this value to set a timeout with the message sending
    const now = new Date();
    const timeoutValue = when.getTime() - now.getTime();
    const timeout = setTimeout(
        () => messageSender.sendMessage(chatId, message),
        timeoutValue,
    );
    this.logger.info(`Timeout set for ${when} - ${when.getTime()}`);
    const jobId = String(now.getTime());
    const scheduledJob: ScheduledJob = {
      chatId,
      id: jobId,
      scheduledFor: when,
      issuedOn: now,
    };

    this.scheduledJobs.set(jobId, { ...scheduledJob, timeout });
    return Promise.resolve(scheduledJob);
  }

  unschedule(jobId: string) {
    const job = this.scheduledJobs.get(jobId);
    if (!job) {
      this.logger.info('No job found for key', jobId);
      return Promise.resolve(false);
    }
    clearTimeout(job.timeout);
    return Promise.resolve(true);
  }

  getJobInfo(jobId: string) {
    const retrievedJob = this.scheduledJobs.get(jobId);
    if (!retrievedJob) return Promise.resolve(undefined);
    const { timeout: _, ...jobDetails } = retrievedJob;
    return Promise.resolve(jobDetails);
  }

  getAllJobs() {
    return Promise.resolve([...this.scheduledJobs.values()]);
  }
}
