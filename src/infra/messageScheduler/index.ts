export interface MessageSender {
  sendMessage: (chatId: string, message: string) => void;
}

export type ScheduleMessagePayload = {
  message: string, // TODO: Support even markdown syntax and buttons
  when: Date,
  chatId: string,
  messageSender: MessageSender,
};

export type ScheduledJob = {
  id: string,
  issuedOn: Date,
  chatId: string,
  scheduledFor: Date,
};

export interface MessageScheduler  {
  schedule(payload: ScheduleMessagePayload) : Promise<ScheduledJob>;
  unschedule(messageJobId: string): Promise<boolean>;
  getJobInfo(messageJobId: string): Promise<ScheduledJob | undefined>;
  getAllJobs(messageJobId: string): Promise<ScheduledJob[]>;
}
